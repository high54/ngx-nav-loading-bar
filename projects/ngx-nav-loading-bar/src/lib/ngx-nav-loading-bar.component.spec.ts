import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxNavLoadingBarComponent } from './ngx-nav-loading-bar.component';

describe('NgxNavLoadingBarComponent', () => {
  let component: NgxNavLoadingBarComponent;
  let fixture: ComponentFixture<NgxNavLoadingBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgxNavLoadingBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxNavLoadingBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
