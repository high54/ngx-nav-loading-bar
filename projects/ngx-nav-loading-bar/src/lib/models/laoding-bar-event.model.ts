import { LoadingBarEventType } from './loading-bar-event-type.enum';

export class LoadingBarEvent {
    constructor(public type: LoadingBarEventType, public value: any) { }
}
