export enum LoadingBarEventType {
    PROGRESS,
    HEIGHT,
    COLOR,
    VISIBLE
}
