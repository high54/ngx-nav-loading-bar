import { Component, OnInit, Input } from '@angular/core';
// Services
import { NgxNavLoadingBarService } from './ngx-nav-loading-bar.service';
// Models
import { LoadingBarEvent } from './models/laoding-bar-event.model';
import { LoadingBarEventType } from './models/loading-bar-event-type.enum';
// Utils
import { isPresent } from './utils/utils';

@Component({
  selector: 'ngx-nav-lb',
  templateUrl: 'ngx-nav-loading-bar.component.html',
  styles: []
})
export class NgxNavLoadingBarComponent implements OnInit {

  @Input() progress: string = '0';
  @Input() color: string = 'firebrick';
  @Input() height: string = '2px';
  @Input() show: boolean = true;

  constructor(public service: NgxNavLoadingBarService) { }

  public ngOnInit(): void {
    this.service.events.subscribe((event: LoadingBarEvent) => {
      if (event.type === LoadingBarEventType.PROGRESS && isPresent(event.value)) {
        this.progress = event.value;
      } else if (event.type === LoadingBarEventType.COLOR) {
        this.color = event.value;
      } else if (event.type === LoadingBarEventType.HEIGHT) {
        this.height = event.value;
      } else if (event.type === LoadingBarEventType.VISIBLE) {
        this.show = event.value;
      }
    });
  }

}
