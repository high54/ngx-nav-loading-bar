import { TestBed } from '@angular/core/testing';

import { NgxNavLoadingBarService } from './ngx-nav-loading-bar.service';

describe('NgxNavLoadingBarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgxNavLoadingBarService = TestBed.get(NgxNavLoadingBarService);
    expect(service).toBeTruthy();
  });
});
