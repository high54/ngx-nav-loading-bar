import { NgModule, ModuleWithProviders } from '@angular/core';
// Components
import { NgxNavLoadingBarComponent } from './ngx-nav-loading-bar.component';
// Services
import { NgxNavLoadingBarService } from './ngx-nav-loading-bar.service';


@NgModule({
  declarations: [NgxNavLoadingBarComponent],
  imports: [
  ],
  exports: [NgxNavLoadingBarComponent],
  providers: [
    NgxNavLoadingBarService
  ]
})
export class NgxNavLoadingBarModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgxNavLoadingBarModule,
      providers: [NgxNavLoadingBarService]
    };
  }
}
