import { Component } from '@angular/core';

@Component({
  selector: 'nav-lb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'NavLoadingBarLibrary';
}
